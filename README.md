First of all, thank you for taking the time to review this project. A few notes of things I wanted to address:
The API key isn't hiddent, as I wanted you to be able to access the project as smoothly as possible.
During the time of this assessment, I had a stomach infection, which is why it took several days longer. Thank you for the chance
to submit this at a later time.
The tests weren't finished. Unfortunately I'm not at 100% yet, and in interest of time I wanted to submit what I had. 
Navigation is in there, but not implemented as it felt slight overkill for two fragments.

What I would've liked:
Fuller error handling, and adding more filtering abilities. 
More UI capacity
Clean up the code to make it a bit more succient.

Once again, thank you for taking the time to review this. Should I go through the round, I would look forward to speaking
with all of you again. 
