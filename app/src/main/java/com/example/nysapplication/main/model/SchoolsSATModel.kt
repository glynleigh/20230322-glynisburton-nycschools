package com.example.nysapplication.main.model

data class SchoolsSATModel (
    var school_name:String,
    val sat_critical_reading_avg_score:String,
    val sat_math_avg_score:String,
    val sat_writing_avg_score:String
)